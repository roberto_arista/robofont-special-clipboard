import vanilla
from mojo.roboFont import OpenWindow
from mojo.UI import CurrentWindow
from mojo.extensions import setExtensionDefault, getExtensionDefault

EXTENSION_KEY = "design.bahman.pasteSpecial"

class PasteSpecialWindow():
	__name__ = 'PasteSpecialWindow'

	def __init__(self):
		self.f = CurrentFont()
		if self.f is not None:
			self.selectedLayer = self.f.layers[0]
			self.w = vanilla.Window((200, 100))
			self.w.contoursCheckbox = vanilla.CheckBox("auto", "Contours", callback=self.contoursCheckboxCallback)
			self.w.rightMarginCheckbox = vanilla.CheckBox("auto", "Right Margin", callback=self.rightMarginCheckboxCallback)
			self.w.leftMarginCheckbox = vanilla.CheckBox("auto", "Left Margin", callback=self.leftMarginCheckboxCallback)
			self.w.leftGroupsCheckbox = vanilla.CheckBox("auto", "Left Side Groups", callback=self.leftGroupsCheckboxCallback)
			self.w.rightGroupsCheckbox = vanilla.CheckBox("auto", "Right Side Groups", callback=self.rightGroupsCheckboxCallback)
			self.w.widthCheckbox = vanilla.CheckBox("auto", "Width", callback=self.widthCheckboxCallback)
			self.w.anchorsCheckbox = vanilla.CheckBox("auto", "Anchors", callback=self.anchorsCheckboxCallback)
			self.w.componentsCheckbox = vanilla.CheckBox("auto", "Components", callback=self.componentsCheckboxCallback)
			self.w.libCheckbox = vanilla.CheckBox("auto", "Lib", callback=self.libCheckboxCallback)
			self.w.layerSelectionButton = vanilla.PopUpButton("auto", [la.name for la in self.f.layers], callback=self.layerSelcectionCallback)
			self.w.replaceCheckbox = vanilla.CheckBox("auto", "Replace existing data", callback=self.replaceCheckboxCallback)
			self.w.cancelButton = vanilla.Button("auto", "Cancel", callback=self.close)
			self.w.pasteSpecial = vanilla.Button("auto", "Paste Special", callback=self.paste)
			rightMarginChecked = getExtensionDefault(EXTENSION_KEY+".rightMargin", True)
			widthChecked = getExtensionDefault(EXTENSION_KEY+".width", False)
			if rightMarginChecked:
				self.w.rightMarginCheckbox.set(True)
				self.w.widthCheckbox.enable(False)
				self.w.widthCheckbox.set(False)
			elif widthChecked:
				self.w.widthCheckbox.set(True)
				self.w.rightMarginCheckbox.enable(False)
				self.w.rightMarginCheckbox.set(False)
			self.w.contoursCheckbox.set(getExtensionDefault(EXTENSION_KEY+".contours", True))
			self.w.leftMarginCheckbox.set(getExtensionDefault(EXTENSION_KEY+".leftMargin", True))
			self.w.anchorsCheckbox.set(getExtensionDefault(EXTENSION_KEY+".anchors", True))
			self.w.componentsCheckbox.set(getExtensionDefault(EXTENSION_KEY+".components", True))
			self.w.libCheckbox.set(getExtensionDefault(EXTENSION_KEY+".lib", True))
			self.w.replaceCheckbox.set(getExtensionDefault(EXTENSION_KEY+".clear", True))

			metrics = dict(
				border=15,
				padding=10,
				moreBorder=70,
			)
			constraints = [
				"H:|-moreBorder-[rightMarginCheckbox]-moreBorder-|",
				"H:|-moreBorder-[leftMarginCheckbox]-moreBorder-|",
				"H:|-moreBorder-[leftGroupsCheckbox]-moreBorder-|",
				"H:|-moreBorder-[rightGroupsCheckbox]-moreBorder-|",
				"H:|-moreBorder-[widthCheckbox]-moreBorder-|",
				"H:|-moreBorder-[anchorsCheckbox]-moreBorder-|",
				"H:|-moreBorder-[componentsCheckbox]-moreBorder-|",
				"H:|-moreBorder-[contoursCheckbox]-moreBorder-|",
				"H:|-moreBorder-[libCheckbox]-moreBorder-|",
				"H:|-[layerSelectionButton]-|",
				"H:|-border-[cancelButton]-padding-[pasteSpecial]-border-|",
				"H:|-border-[replaceCheckbox]-border-|",
				"V:|-border-[contoursCheckbox][componentsCheckbox][leftMarginCheckbox][rightMarginCheckbox][leftGroupsCheckbox][rightGroupsCheckbox][widthCheckbox][anchorsCheckbox][libCheckbox][layerSelectionButton][replaceCheckbox]-padding-[cancelButton]-border-|",
				"V:|-border-[contoursCheckbox][componentsCheckbox][leftMarginCheckbox][rightMarginCheckbox][leftGroupsCheckbox][rightGroupsCheckbox][widthCheckbox][anchorsCheckbox][libCheckbox][layerSelectionButton][replaceCheckbox]-padding-[pasteSpecial]-border-|"
			]

			self.w.addAutoPosSizeRules(constraints, metrics=metrics)
			self.w.center()
			self.w.open()
			self.w.bind("resigned key", self.closeResign)

	def layerSelcectionCallback(self, sender):
		self.selectedLayer = self.f.layers[sender.get()]

	def replaceCheckboxCallback(self, sender):
		setExtensionDefault(EXTENSION_KEY+".clear", sender.get())

	def rightMarginCheckboxCallback(self, sender):
		checked = sender.get()
		if checked:
			self.w.widthCheckbox.enable(False)
			self.w.widthCheckbox.set(False)
		else:
			self.w.widthCheckbox.enable(True)
		setExtensionDefault(EXTENSION_KEY+".rightMargin", checked)

	def widthCheckboxCallback(self, sender):
		checked = sender.get()
		if checked:
			self.w.rightMarginCheckbox.enable(False)
			self.w.rightMarginCheckbox.set(False)
		else:
			self.w.rightMarginCheckbox.enable(True)
		setExtensionDefault(EXTENSION_KEY+".width", checked)

	def contoursCheckboxCallback(self, sender):
		setExtensionDefault(EXTENSION_KEY+".contours", sender.get())

	def leftMarginCheckboxCallback(self, sender):
		setExtensionDefault(EXTENSION_KEY+".leftMargin", sender.get())

	def leftGroupsCheckboxCallback(self, sender):
		setExtensionDefault(EXTENSION_KEY+".leftGroup", sender.get())

	def rightGroupsCheckboxCallback(self, sender):
		setExtensionDefault(EXTENSION_KEY+".rightGroup", sender.get())

	def anchorsCheckboxCallback(self, sender):
		setExtensionDefault(EXTENSION_KEY+".anchors", sender.get())

	def componentsCheckboxCallback(self, sender):
		setExtensionDefault(EXTENSION_KEY+".components", sender.get())

	def libCheckboxCallback(self, sender):
		setExtensionDefault(EXTENSION_KEY+".lib", sender.get())

	def paste(self, sender):
		self.pasteContours = self.w.contoursCheckbox.get() == 1
		self.pasteComponents = self.w.componentsCheckbox.get() == 1
		self.pasteAnchors = self.w.anchorsCheckbox.get() == 1
		self.pasteLeftMargin = self.w.leftMarginCheckbox.get() == 1
		self.pasteRightMargin = self.w.rightMarginCheckbox.get() == 1
		self.pasteLeftGroups = self.w.leftGroupsCheckbox.get() == 1
		self.pasteRightGroups = self.w.rightGroupsCheckbox.get() == 1
		self.pasteWidth = self.w.widthCheckbox.get() == 1
		self.pasteLib = self.w.libCheckbox.get() == 1
		self.replaceData = self.w.replaceCheckbox.get() == 1

		windowName = CurrentWindow().doodleWindowName
		if windowName == 'FontWindow':
			self.clipboardGlyphs = {}  # int(unicode): RGlyph, string(glyph.name): RGlyph
			for g in self.f.clipboardGlyphs:
				if g.unicodes != ():
					for v in g.unicodes:
						self.clipboardGlyphs[v] = g
				self.clipboardGlyphs[g.name] = g
			self.targetGlyphNames = set()
			if self.f.templateSelectedGlyphNames == ():
				self.targetGlyphNames = set(self.f.keys())
			else:
				self.targetGlyphNames = set(self.f.templateSelectedGlyphNames)
			for gn in self.targetGlyphNames:
				g = self.f[gn]
				if len(self.f.clipboardGlyphs) == 1:
					sourceGlyph = self.f.clipboardGlyphs[0]
				else:
					sourceGlyph = self._getSourceGlyph(g)
				if sourceGlyph is not None and sourceGlyph != g:
					if gn not in self.selectedLayer:
						targetGlyph = self.selectedLayer.newGlyph(gn)
					else:
						targetGlyph = self.selectedLayer[gn]
					self._pasteToGlyph(sourceGlyph, targetGlyph)
			self.f.changed()
		elif windowName == 'GlyphWindow':
			if len(self.f.clipboardGlyphs) == 1:
				g = CurrentGlyph()
				if g.name not in self.selectedLayer:
					targetGlyph = self.selectedLayer.newGlyph(g.name)
				else:
					targetGlyph = self.selectedLayer[g.name]
				self._pasteToGlyph(self.f.clipboardGlyphs[0], targetGlyph)
		self.w.close()

	def _getSourceGlyph(self, targetGlyph):
		for v in targetGlyph.unicodes:
			if self.clipboardGlyphs.get(v, None) is not None:
				return self.clipboardGlyphs[v]
		return self.clipboardGlyphs.get(targetGlyph.name, None)

	def _pasteToGlyph(self, sourceGlyph, targetGlyph):
		targetGlyph.prepareUndo('Paste Special')
		if self.pasteContours:
			if self.replaceData:
				targetGlyph.clear(contours=True, components=False, anchors=False)
			for c in sourceGlyph.contours:
				targetGlyph.appendContour(c)
		if self.pasteComponents:
			if self.replaceData:
				targetGlyph.clear(contours=False, components=True, anchors=False)
			for c in sourceGlyph.components:
				newC = targetGlyph.appendComponent(component=c)
				newC.offset = c.offset
				newC.scale = c.scale
		if self.pasteLeftMargin:
			targetGlyph.leftMargin = sourceGlyph.leftMargin
		if self.pasteRightMargin and not self.pasteWidth:
			targetGlyph.rightMargin = sourceGlyph.rightMargin
		elif self.pasteWidth:
			targetGlyph.width = sourceGlyph.width
		if self.pasteAnchors:
			if self.replaceData:
				targetGlyph.clear(contours=False, components=False, anchors=True)
			for a in sourceGlyph.anchors:
				targetGlyph.appendAnchor(anchor=a)
		if self.pasteLib:
			if self.replaceData:
				targetGlyph.lib.clear()
			targetGlyph.lib.update(sourceGlyph.lib)
		targetGlyph.changed()
		targetGlyph.performUndo()

	def close(self, sender):
		self.w.close()

	def closeResign(self, sender):
		if self.w.isVisible():
			self.w.close()

if __name__ == '__main__':
	OpenWindow(PasteSpecialWindow)
