from mojo.events import addObserver, removeObserver
from defconAppKit.windows.baseWindow import BaseWindowController
from mojo.roboFont import RFont

class _SpecialClipboard():

	def __init__(self):
		addObserver(self, '_fontOverviewCopy', 'fontOverviewCopy')
		addObserver(self, '_copy', 'copy')

	def destroy(self, sender):
		removeObserver(self, 'fontOverviewCopy')
		removeObserver(self, 'copy')

	def _fontOverviewCopy(self, info):
		RFont.clipboardGlyphs = info["glyphs"]

	def _copy(self, info):
		RFont.clipboardGlyphs = [info["glyph"], ]

clipboard = _SpecialClipboard()
debug = False
if debug:
	from vanilla import FloatingWindow
	class DebuggerWindow(BaseWindowController):
		def __init__(self):
			self.w = FloatingWindow((500, 500), f'clipboard debug!')
			self.w.open()
			self.w.bind("close", clipboard.destroy)
	DebuggerWindow()